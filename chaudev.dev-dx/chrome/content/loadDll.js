
/*
 *    DLLChaudevLoader -- loads chaudev_s.dll and makes initial call to SetDeviceID
 */

var DLLChaudevLoader = function() {

  if ("undefined" == typeof(@gBrowser@.DLLChaudev)) {

    dump ("DLLChaudevLoader: dll loading... context: @gBrowser@.DLLChaudev \n");
    var lDLLChaudev = {};
    Components.utils.import("resource://gre/modules/AddonManager.jsm");
    AddonManager.getAddonByID("chaudev@roamingquark.info", function(addon)
    {
      var uri = addon.getResourceURI("src/chaudev_s.dll");
      if (uri instanceof Components.interfaces.nsIFileURL)
      {

/*LIBRARY   CHAUDEV_S
  EXPORTS
   GetDevicesNumber   1
   GetDeviceName      2
   GetDeviceID        3
   SetDeviceID        4 */

        lDLLChaudev.lib = ctypes.open(uri.file.path);      
        lDLLChaudev.GetDevicesNumber = lDLLChaudev.lib.declare("GetDevicesNumber",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t        /* return type */
                                                                    );       /* item hit */
 /*unsigned int GetDeviceName(const char **name, int number) */
        lDLLChaudev.GetDeviceName = lDLLChaudev.lib.declare("GetDeviceName",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.int32_t,        /* return type */
                                                             ctypes.char.ptr.ptr,    /* arg 1 */
                                                             ctypes.uint32_t        /* arg 2 */
                                                                    );       /* item hit */
        lDLLChaudev.GetDeviceID = lDLLChaudev.lib.declare("GetDeviceID",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t        /* return type */
                                                                    );       /* item hit */
        lDLLChaudev.SetDeviceID = lDLLChaudev.lib.declare("SetDeviceID",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t,        /* return type */
                                                             ctypes.uint32_t        /* arg 1 */
                                                                    );       /* item hit */
    //    dump ("set deviceid: " + typeof (lDLLChaudev.SetDeviceID) + "\n");

        let DLLChaudev = lDLLChaudev;
        //get pref
        let prefs = Components.classes["@mozilla.org/preferences-service;1"]
                    .getService(Components.interfaces.nsIPrefService);
        prefs = prefs.getBranch("extensions.chaudev.");
  
        let pval = prefs.getCharPref("device");

        let ipval = parseInt(pval);

        if (("undefined" == typeof(prefs)) || ("undefined" == typeof(pval))) {
          ipval = 4294967295;
          pval = "4294967295";
        };
    
        let r = DLLChaudev.SetDeviceID(ipval);
        dump ("DLLChaudevLoader: set initial device to " + ipval + " (" + r + ")\n");



      }
    });

    @gBrowser@.DLLChaudev = lDLLChaudev;
  } 
}

/*
 *   DLLChaudevGet - gets global interface to chaudev_s.dll
 */
var DLLChaudevGet = function() {
  return @gBrowser@.DLLChaudev;
}


