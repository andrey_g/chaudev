#define UNICODE
#define _UNICODE
#include <windows.h>
#include <tchar.h>
//#include <stdio.h>
#include "patcher_s.h"

#include "patcher.h"
#include "dsound_int.h"

typedef unsigned int uint32_t;
#define NS_ERROR_FAILURE 2

BOOL WINAPI DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID lpReserved )  // reserved
{
    // Perform actions based on the reason for calling.
    switch( fdwReason ) 
    { 
        case DLL_PROCESS_ATTACH:
         // Initialize once for each new process.
         // Return FALSE to fail DLL load.
            dsound_Init();
            break;

        case DLL_THREAD_ATTACH:
         // Do thread-specific initialization.
            break;

        case DLL_THREAD_DETACH:
         // Do thread-specific cleanup.
            break;

        case DLL_PROCESS_DETACH:
         // Perform any necessary cleanup.
            break;
    }
    return TRUE;  // Successful DLL_PROCESS_ATTACH.
}


extern "C"
unsigned int GetDevicesNumber()
{

    WAVEOUTCAPS woc;

    UINT num = waveOutGetNumDevs();    

    if (num <= 0) 
        return 0;


    uint32_t act_num = 0;
    for (int i = 0; i < num; i++) {
        MMRESULT res = waveOutGetDevCaps (i, &woc, sizeof (woc));
        if (res == MMSYSERR_NOERROR) {
            act_num++;            
        }
    }

  return act_num;    
}

const int GetDeviceName_res_len = 1024;
static char GetDeviceName_res[GetDeviceName_res_len];

extern "C"
int GetDeviceName(const char **name, int number) 
{
    *name="error\0";
    WAVEOUTCAPS woc;

    UINT num = waveOutGetNumDevs();    

    if (num <= 0) 
        return PT_ERROR;


    uint32_t act_num = 0;
    for (int i = 0; i < num; i++) {
        MMRESULT res = waveOutGetDevCaps (i, &woc, sizeof (woc));
        if (res == MMSYSERR_NOERROR) {
            act_num++;            
        }
    }

    if (number > act_num) 
        return PT_INVALID_PARAMS;

    uint32_t act_ind = 0;
    for (int i = 0; i < num; i++) {
        memset(&woc, 0, sizeof(woc));
        memset (GetDeviceName_res, 0, GetDeviceName_res_len);
        MMRESULT res = waveOutGetDevCaps (i, &woc, sizeof (woc));
        if (res == MMSYSERR_NOERROR) {
//            MessageBox(0,woc.szPname,L"Here",MB_OK);
            //convert string
            if (number == act_ind) {

              wchar_t tmp[1024];
              wchar_t *ptmp;
              if (dsound_MapWinmmName (woc.szPname, tmp, 1024)) {
                ptmp = tmp;
              }
              else{
                ptmp = woc.szPname;
              }

              int res_len = WideCharToMultiByte(CP_UTF8, 0, ptmp, _tcslen(ptmp), 0,0,0,0);
              if (res_len == 0) {
                *name = "Utf-8 conversion failed\0";
              }            
              else {
                 if (!WideCharToMultiByte( 
                          CP_UTF8,                // convert to UTF-8 
                          0,                      // default flags 
                          ptmp,           // source UTF-16 string 
                          _tcslen(ptmp),         // source string length, in wchar_t's, 
                          GetDeviceName_res,               // destination buffer 
                          GetDeviceName_res_len,          // destination buffer size, in chars 
                         NULL, NULL              // unused 
                    ) ) {
                   *name = "Utf-8 conversion failed(2)\0";
                 }
                 else {
                   *name = GetDeviceName_res;
                   //*(name+res_len) = 0;
                 }
              }
              return i;
            }
            act_ind++;
        }
    }

    *name = "Invalid index\0";

    return PT_ERROR;
}
