#ifndef PATCHER_S_H
#define PATCHER_S_H

#define PT_ERROR 0x10000
#define PT_INVALID_PARAMS (PT_ERROR+1)
#define PT_VP_FAILED (PT_ERROR+2)
#define PT_OK 0

#endif//PATCHER_S_H