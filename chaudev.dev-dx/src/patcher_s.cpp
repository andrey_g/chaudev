#include <windows.h>
#include <Psapi.h>
#include <stdlib.h>
#include "patcher_s.h"

//#define DBG

#ifdef DBG
#include <stdio.h>
//#define dbg_log(args) do  { int lasterr = GetLastError(); printf args ; } while(0)
#define mbprintf(...) do { char temp[256];sprintf(temp,__VA_ARGS__);MessageBox(0,temp,"msg",MB_OK); } while(0)
#define dbg_log(args) do  { int lasterr = GetLastError(); mbprintf args ; } while(0)
#else
#define dbg_log(args)
#endif




const int header_gkmedias_seq_size = 15;

static unsigned char header_gkmedias_seq_mask[] = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
}; //3C A5 49 01 

static unsigned char header_gkmedias_seq[] = {
  0x8D, 0x45, 0xD4, 0x50, 0x6A, 0xFF, 0x8D, 0xBB, 0xB4, 0x00, 0x00, 0x00, 0x57, 0xFF, 0x15
}; //3C A5 49 01 
// patchbyte : header_ptr + 5


const int header_3xAVCore_seq_size = 18;

static unsigned char header_3xAVCore_seq_mask[] = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
}; //3C A5 49 01 

static unsigned char header_3xAVCore_seq[] = {
  0xEB, 0x02, 0x33, 0xFF, 0xC7, 0x45, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0x8B, 0x17, 0x8B, 0x42, 0x24, 0x6A, 0xFF
}; //3C A5 49 01 
// patchbyte : header_ptr + 17


int memcmp_masked(unsigned char *src, 
                  unsigned char *src_mask,
                  unsigned char *dst, 
                  unsigned char *dst_mask, 
                  int size)
{
    for (int i = 0; i < size; i++) {
        int diff = (src[i] & src_mask[i]) - (dst[i] & dst_mask[i]);
	if (diff) return diff;
    }
    return 0;
}



static CRITICAL_SECTION cs;


void *FindWhere ();

void *addr = 0;
void *addr2 = 0;



extern "C"
unsigned int GetDeviceID ()
{
  if (!addr){
     addr = FindWhere ();
    if (!addr){
      return PT_ERROR;//error
    }
  }

  unsigned char did = *((unsigned char*)(addr));

  unsigned int dev_id = (did == 255) ? 0xffffffff : did;

  return dev_id;
}

static int wp_set (void *addr, unsigned int dev_id)
{
  //code region is write-protected
  DWORD old_protect;
  if (!VirtualProtect(addr, 1, PAGE_EXECUTE_READWRITE, &old_protect)) {
    dbg_log (("VirtualProtect failed %ld (0x%lX)",lasterr,lasterr));
    return PT_VP_FAILED;
  }
  *((unsigned char*)(addr)) = dev_id & 0xff;
  VirtualProtect(addr, 1, old_protect, &old_protect);
  return PT_OK;
}

extern "C"
int SetDeviceID (unsigned int dev_id)
{
  if (!addr){
     addr = FindWhere ();
    if (!addr){
      return PT_ERROR;
    }
  }

  dbg_log (("FindWhere returned 0x%lX",(unsigned long)(addr)));
  int res = wp_set(addr, dev_id);
  if (res != PT_OK) return res;

  if (addr2) wp_set(addr2, dev_id);

  return PT_OK;
}

static int ScanAddressesForSequence (LPVOID lpBaseOfDll, DWORD SizeOfImage,
                                     unsigned char *src_mask,
                                     unsigned char *header_seq, 
                                     unsigned char *header_seq_mask, 
                                     int header_seq_size,
                                     int return_pointer_offset_of_WAVEMAPPER_value_of_PUSH_instruction,
                                     void **pres,
                                     bool check_pwaveOutOpen,
                                     void *pwaveOutOpen,
                                     bool check_3xAVCore, LPVOID lpBaseOf3xAVCore, DWORD SizeOf3xAVCore)
{
  for (unsigned char *i = (unsigned char *)lpBaseOfDll; i <((unsigned char *)lpBaseOfDll + SizeOfImage - header_seq_size); i++) {
    __try
    {
      if(!memcmp_masked(i, src_mask, header_seq, header_seq_mask, header_seq_size)) {
        //now check for false positive
        if (!check_pwaveOutOpen) {
            (*pres) = (void *)(i+return_pointer_offset_of_WAVEMAPPER_value_of_PUSH_instruction);      
            return 1;
        } else {

          void *p = **((void***)(i+header_seq_size));
          dbg_log (("checking [0x%lX] = 0x%lX, [0x%lX] = 0x%lX\n", i+header_seq_size, *((unsigned long *)(i+header_seq_size)), *((unsigned long *)(i+header_seq_size)),
                            *(unsigned long *)(*((unsigned long *)(i+header_seq_size)))));

          if (
/*winmm.dll    :*/ (p == pwaveOutOpen)
             ) {
              (*pres) = (void *)(i+return_pointer_offset_of_WAVEMAPPER_value_of_PUSH_instruction /*5*/);      
              return 1;
          }
          if (
                  (check_3xAVCore) 
               && (p > lpBaseOf3xAVCore) 
               && (p < ((unsigned char *)lpBaseOf3xAVCore + SizeOf3xAVCore))
             ) {
              (*pres) = (void *)(i+return_pointer_offset_of_WAVEMAPPER_value_of_PUSH_instruction /*5*/);
              return 2;
          }
        }
// /*monkeypatched:*/ (((unsigned long)p > 0x400000) && ((unsigned long)p < 0x10000000)) 
      }
    }
    __except((GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION) ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH)
    {
      dbg_log (("exception at %lX\n", i));
    }
  }
  return 0;
}

static void *FindWhere ()
{  
  HMODULE hWinmm = LoadLibrary ("winmm.dll");
  if (!hWinmm) {
    dbg_log(("LoadLibrary winmm.dll failed %ld(%lX)\n",lasterr,lasterr));
    return 0;
  }
  HMODULE hMedias = LoadLibrary ("gkmedias.dll");
  if (!hMedias) {
    dbg_log(("LoadLibrary gkmedias.dll failed %ld(%lX)\n",lasterr,lasterr));
    FreeLibrary(hWinmm);
    return 0;
  }
  void *pwaveOutOpen = GetProcAddress (hWinmm,"waveOutOpen");
  if (!pwaveOutOpen) {
    dbg_log(("GetProcAddress waveOutOpen failed %ld(%lX)\n",lasterr,lasterr));
    FreeLibrary(hWinmm);
    FreeLibrary(hMedias);
    return 0;
  }

  MODULEINFO mi;
  memset(&mi, 0, sizeof(mi));
  if (!GetModuleInformation (GetCurrentProcess(), hMedias, &mi, sizeof(mi))) {    
    dbg_log(("GetModuleInformation failed %ld(%lX)\n",lasterr,lasterr));
    FreeLibrary(hWinmm);
    FreeLibrary(hMedias);
    return 0;
  }


  HMODULE h3xAVCore = GetModuleHandle ("3xAVCore.dll");
  MODULEINFO mi_3xAVCore;
  memset(&mi_3xAVCore, 0, sizeof(mi_3xAVCore));
  if (!h3xAVCore) {
    dbg_log(("LoadLibrary h3xAVCore.dll failed %ld(%lX)\n",lasterr,lasterr));
//    FreeLibrary(hWinmm);
//    return 0;
  } else {
    if (!GetModuleInformation (GetCurrentProcess(), h3xAVCore, &mi_3xAVCore, sizeof(mi_3xAVCore))){    
        dbg_log(("GetModuleInformation (h3xAVCore) failed %ld(%lX)\n",lasterr,lasterr));
        FreeLibrary(h3xAVCore);
        h3xAVCore = 0;
    }
  }


  void *res=0;
  addr2 = 0; //zero aux address
  int scan_res = 
     ScanAddressesForSequence (mi.lpBaseOfDll, mi.SizeOfImage, 
                               header_gkmedias_seq_mask, header_gkmedias_seq, 
                               header_gkmedias_seq_mask, header_gkmedias_seq_size, 5,
                               &res, true, pwaveOutOpen, (h3xAVCore!=0), mi_3xAVCore.lpBaseOfDll, mi_3xAVCore.SizeOfImage);

  if ((scan_res == 2) && (h3xAVCore)) { //3xAVCore have made some patching, so we will patch 3xAVCore instead
     addr2 = res;
     ScanAddressesForSequence (mi_3xAVCore.lpBaseOfDll, mi_3xAVCore.SizeOfImage, 
                               header_3xAVCore_seq_mask, header_3xAVCore_seq, 
                               header_3xAVCore_seq_mask, header_3xAVCore_seq_size, 17,
                               &res, false, 0, false, 0, 0);
  }

  FreeLibrary(hWinmm);
  FreeLibrary(hMedias);
//  if (h3xAVCore) 
//      FreeLibrary(h3xAVCore);

  return (void *)res;
}