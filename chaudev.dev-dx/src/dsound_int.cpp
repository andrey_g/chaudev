#define UNICODE
#define _UNICODE

#define INITGUID

#include <string.h>
#include <dsound.h>
#include <dsconf.h>
#include <Unknwn.h>

//#define DBG

#ifdef DBG
#include <stdio.h>
//#define dbg_log(args) do  { int lasterr = GetLastError(); printf args ; } while(0)
#define mbprintf(...) do { char temp[256];sprintf(temp,__VA_ARGS__);MessageBoxA(0,temp,"msg",MB_OK); } while(0)
#define dbg_log(args) do  { int lasterr = GetLastError(); mbprintf args ; } while(0)
#else
#define dbg_log(args)
#endif


typedef HRESULT  (CALLBACK * DLLGETCLASSOBJECTPROC)(REFCLSID, REFIID, LPVOID *);

static bool dsound_int_initialized = false;
static HMODULE hDsound = 0;
static DLLGETCLASSOBJECTPROC fDllGetClassObject = 0;
static IKsPropertySet * pps = 0;


typedef  struct {
  wchar_t *res_str;
  unsigned long bsize;
  GUID guid;
} dsenum_callback_context;

static BOOL CALLBACK dsenum_callback (DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION_DATA *d, dsenum_callback_context *ctx) {
  if ((d->DataFlow == DIRECTSOUNDDEVICE_DATAFLOW_RENDER) && (memcmp(&(d->DeviceId), &ctx->guid, sizeof (GUID)) == 0)) {
    wcsncpy(ctx->res_str,d->Description,ctx->bsize);
    return 0;
  }
  return 1;
}

int dsound_Init ()
{
  //DirectSound dynamic loading (thread not very safe, actually)
  if (!dsound_int_initialized) {
    hDsound = LoadLibrary(L"dsound.dll");
    if (hDsound==0) {
      dbg_log(("LoadLibrary  failed "));
      return 0;
    }
    fDllGetClassObject = (DLLGETCLASSOBJECTPROC)GetProcAddress(hDsound, "DllGetClassObject");
    if (fDllGetClassObject == 0) {
      dbg_log(("GetProcAddress failed "));
      return 0;
    }

    HRESULT hr;
    IClassFactory * pcf = 0;
    ULONG support;

    hr = (fDllGetClassObject)(CLSID_DirectSoundPrivate, IID_IClassFactory, (void **)(&pcf));
    if (pcf==0) {
      dbg_log(("DllGetClassObject DirectSoundPrivate failed: 0x%lX", hr));
      return 0; //fail to get DirectSoundPrivate
    }

  /* direct sound private does have an IKsPropertySet */
    hr = pcf->CreateInstance(NULL, IID_IKsPropertySet, (void **)(&pps));
    if (hr!=DS_OK) {
      dbg_log(("CreateInstance IKsPropertySet failed: 0x%lX", hr));
      return 0;
    }

    hr = pps->QuerySupport(DSPROPSETID_DirectSoundDevice, DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING, &support);
    if (hr!=DS_OK) {
      dbg_log(("QuerySupport DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING failed: 0x%lX", hr));
      return 0;
    }

    if (!(support & KSPROPERTY_SUPPORT_GET)) {
      dbg_log(("DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING -- not supported"));
      return 0;
    }

    hr = pps->QuerySupport(DSPROPSETID_DirectSoundDevice, DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION, &support);
    if (hr!=DS_OK) {
      dbg_log(("QuerySupport DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION failed: 0x%lX", hr));
      return 0;
    }

    if (!(support & KSPROPERTY_SUPPORT_GET)) {
      dbg_log(("DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION -- not supportsed"));
      return 0;
    }


  }

  dsound_int_initialized = true;

  return 1;

}


int dsound_MapWinmmName (wchar_t *winmm_name_in, wchar_t *dsound_name_out, unsigned long bsize)
{
  *(dsound_name_out) = 0;
  HRESULT hr = 0;
  if (!dsound_int_initialized) {
    return 0;
  }

  DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING_DATA 
        WaveDeviceMap;

  WaveDeviceMap.DeviceName = winmm_name_in; 
  WaveDeviceMap.DataFlow = DIRECTSOUNDDEVICE_DATAFLOW_RENDER; 

  DWORD bytes = 0;

  hr = pps->Get(DSPROPSETID_DirectSoundDevice, 
					  DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING,
					  NULL,
					  0,
					  &WaveDeviceMap, 
					  sizeof(WaveDeviceMap), 
					  &bytes);
  if ((hr != DS_OK) || (bytes == 0)) {
    dbg_log(("get property DSPROPERTY_DIRECTSOUNDDEVICE_WAVEDEVICEMAPPING failed: 0x%lX",hr));
    return 0;
  }

  #define destruct_guid(guid)  guid.Data1, guid.Data2, guid.Data3,  \
                               guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3], guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]


/*  DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION_DATA 
        dsDescrData;
  memset (&dsDescrData, 0, sizeof(dsDescrData));

  dsDescrData.DeviceId = WaveDeviceMap.DeviceId;
  dsDescrData.DataFlow = DIRECTSOUNDDEVICE_DATAFLOW_RENDER;

  //dbg_log(("guid = {%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX}",destruct_guid(dsDescrData.DeviceId)));

  bytes = 0;

  hr = pps->Get(DSPROPSETID_DirectSoundDevice, 
					  DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION,
					  NULL,
					  0,
					  &dsDescrData, 
					  sizeof(dsDescrData), 
					  &bytes);
  if ((hr != DS_OK) || (bytes == 0)) {
    dbg_log(("get property DSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION failed: 0x%lX",hr));
    return 0;
  }

  dbg_log(("dsound_name_out = 0x%lX, *dsound_name_out = 0x%lX", dsound_name_out, *dsound_name_out));*/
//  (*dsound_name_out) = dsDescrData.Module;
//  dbg_log(("dsound_name_out = 0x%lX, *dsound_name_out = 0x%lX", dsound_name_out, *dsound_name_out));
//  dbg_log(("dsDescrData.Descritpion = 0x%lx", dsDescrData.WaveDeviceId));

  DSPROPERTY_DIRECTSOUNDDEVICE_ENUMERATE_DATA enumData;

  dsenum_callback_context dcc;
  dcc.res_str = dsound_name_out;
  dcc.bsize = bsize;
  dcc.guid = WaveDeviceMap.DeviceId;

  enumData.Callback = (LPFNDIRECTSOUNDDEVICEENUMERATECALLBACK)dsenum_callback;
  enumData.Context = &dcc;

  hr = pps->Get(DSPROPSETID_DirectSoundDevice, 
					  DSPROPERTY_DIRECTSOUNDDEVICE_ENUMERATE,
					  NULL,
					  0,
					  &enumData, 
					  sizeof(enumData), 
					  &bytes);

  return 1;
}