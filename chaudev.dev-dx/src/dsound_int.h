#ifndef DSOUND_INT_H
#define DSOUND_INT_H

int dsound_Init ();

int dsound_MapWinmmName (wchar_t *winmm_name_in, wchar_t *dsound_name_out, unsigned long bsize);

#endif //DSOUND_INT_H