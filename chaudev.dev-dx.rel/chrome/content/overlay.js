/**
 * XULChaudev namespace.
 */
try{//catch global error

Components.utils.import("resource://gre/modules/ctypes.jsm");

if ("undefined" == typeof(XULChaudev)) {
  var XULChaudev = {};
};


//_include loadDll.js

/*
 *    DLLChaudevLoader -- loads chaudev_s.dll and makes initial call to SetDeviceID
 */

var DLLChaudevLoader = function() {

  if ("undefined" == typeof(gBrowser.DLLChaudev)) {

    dump ("DLLChaudevLoader: dll loading... context: gBrowser.DLLChaudev \n");
    var lDLLChaudev = {};
    Components.utils.import("resource://gre/modules/AddonManager.jsm");
    AddonManager.getAddonByID("chaudev@roamingquark.info", function(addon)
    {
      var uri = addon.getResourceURI("src/chaudev_s.dll");
      if (uri instanceof Components.interfaces.nsIFileURL)
      {

/*LIBRARY   CHAUDEV_S
  EXPORTS
   GetDevicesNumber   1
   GetDeviceName      2
   GetDeviceID        3
   SetDeviceID        4 */

        lDLLChaudev.lib = ctypes.open(uri.file.path);      
        lDLLChaudev.GetDevicesNumber = lDLLChaudev.lib.declare("GetDevicesNumber",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t        /* return type */
                                                                    );       /* item hit */
 /*unsigned int GetDeviceName(const char **name, int number) */
        lDLLChaudev.GetDeviceName = lDLLChaudev.lib.declare("GetDeviceName",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.int32_t,        /* return type */
                                                             ctypes.char.ptr.ptr,    /* arg 1 */
                                                             ctypes.uint32_t        /* arg 2 */
                                                                    );       /* item hit */
        lDLLChaudev.GetDeviceID = lDLLChaudev.lib.declare("GetDeviceID",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t        /* return type */
                                                                    );       /* item hit */
        lDLLChaudev.SetDeviceID = lDLLChaudev.lib.declare("SetDeviceID",       /* function name */
                                                             ctypes.default_abi,    /* ABI type */
                                                             ctypes.uint32_t,        /* return type */
                                                             ctypes.uint32_t        /* arg 1 */
                                                                    );       /* item hit */
    //    dump ("set deviceid: " + typeof (lDLLChaudev.SetDeviceID) + "\n");

        let DLLChaudev = lDLLChaudev;
        //get pref
        let prefs = Components.classes["@mozilla.org/preferences-service;1"]
                    .getService(Components.interfaces.nsIPrefService);
        prefs = prefs.getBranch("extensions.chaudev.");
  
        let pval = prefs.getCharPref("device");

        let ipval = parseInt(pval);

        if (("undefined" == typeof(prefs)) || ("undefined" == typeof(pval))) {
          ipval = 4294967295;
          pval = "4294967295";
        };
    
        let r = DLLChaudev.SetDeviceID(ipval);
        dump ("DLLChaudevLoader: set initial device to " + ipval + " (" + r + ")\n");



      }
    });

    gBrowser.DLLChaudev = lDLLChaudev;
  } 
}

/*
 *   DLLChaudevGet - gets global interface to chaudev_s.dll
 */
var DLLChaudevGet = function() {
  return gBrowser.DLLChaudev;
}



DLLChaudevLoader();


/**
 * Controls the browser overlay for the chaudev extension.
 */
XULChaudev.BrowserOverlay = {
  /**
   * 
   */

  startup : function() {
    DLLChaudevLoader();
  },
  
  shutdown : function() {
  },

  sayHello : function(aEvent) {
    let stringBundle = document.getElementById("bundle_chaudev");
    let message = stringBundle.getString("chaudev.chaudev.label");

    window.alert(message);
  },

  checkMenu : function(aEvent) {

    var cp = document.getElementById("menu_tools-chaudev-popup");
    let msg = cp.lastChild.label;
    dump(msg);

    try {
        let DLLChaudev = DLLChaudevGet();


        let cnum = DLLChaudev.GetDevicesNumber();
        dump("XULChaudev.BrowserOverlay.checkMenu: cnum = " + cnum + "\n");
        let c = 0;
        let dvs = [];


        while (c < cnum) {
          let dname = new ctypes.char.ptr();
          let dnum = DLLChaudev.GetDeviceName(dname.address(),c);
          dump("XULChaudev.BrowserOverlay.checkMenu: dname["+c+"] = " + dnum + "\n");
          dvs.push({szPname:dname.readString(),DeviceID:dnum});
          c = c + 1;
        }

        dump("XULChaudev.BrowserOverlay.checkMenu: dvs = " + dvs + "\n");

        var did = DLLChaudev.GetDeviceID();
        dump("XULChaudev.BrowserOverlay.checkMenu: did = " + DLLChaudev.GetDeviceID() + "\n");


        while (cp.hasChildNodes()){
          cp.removeChild(cp.firstChild);
        }

        var chkd = 0;

        if (did == 4294967295) chkd = 1;

        var t = XULChaudev.BrowserOverlay.createRadioMenuItem("Default (original)","menu_tools-chaudev-popup-radio",
                                            "XULChaudev.BrowserOverlay.setCurDevice(4294967295);",chkd);
        cp.appendChild(t);

        for (var i in dvs) {
              chkd = 0;
              if (did == dvs[i].DeviceID) 
                  chkd = 1;
              t = XULChaudev.BrowserOverlay.createRadioMenuItem(dvs[i].szPname,"menu_tools-chaudev-popup-radio",
                                            "XULChaudev.BrowserOverlay.setCurDevice("+dvs[i].DeviceID+");",chkd);
              cp.appendChild(t);
        }


    } catch (anError) {
            dump("XULChaudev.BrowserOverlay.checkMenu: ERROR: " + anError);
    }
  },

  createRadioMenuItem:function(aLabel,aName,onCommand,checked) {
    const XUL_NS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
    var item = document.createElementNS(XUL_NS, "menuitem"); // create a new XUL menuitem
    item.setAttribute("label", aLabel);
    item.setAttribute("type", "radio");
    item.setAttribute("name", aName);
    item.setAttribute("oncommand", onCommand);
    if (checked == 1) {
       item.setAttribute("checked","true");
    }
    return item;
  },
  setCurDevice:function(dev_id) {
    try{
    let DLLChaudev = DLLChaudevGet();
    DLLChaudev.SetDeviceID(1*dev_id);

// save preference
      let prefs = Components.classes["@mozilla.org/preferences-service;1"]
                      .getService(Components.interfaces.nsIPrefService);
      prefs = prefs.getBranch("extensions.chaudev.");

      prefs.setCharPref("device", "" + dev_id);


    } catch (anError) {
        dump("XULChaudev.BrowserOverlay.setCurDevice: ERROR(setCurDevice): " + anError);
    }
  }
};

window.addEventListener("load", function(e) { XULChaudev.BrowserOverlay.startup(); }, false);
window.addEventListener("unload", function(e) { XULChaudev.BrowserOverlay.shutdown(); }, false);

} catch (anError) {
  dump("overlay.js.template: ERROR(global): " + anError + "\n");
}
